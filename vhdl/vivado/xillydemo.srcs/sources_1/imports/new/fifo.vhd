library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity fifo is
    GENERIC (
        ADDRESS_WIDTH : integer:=8;
        DATA_WIDTH : integer:=32
    );

    port ( clk : in std_logic;
        reset : in std_logic;
        enr : in std_logic;
        enw : in std_logic;
        dataout : out std_logic_vector(DATA_WIDTH-1 downto 0);
        datain : in std_logic_vector (DATA_WIDTH-1 downto 0);
        empty : out std_logic;
        full : out std_logic;
        startMagic: in std_logic
    );
end fifo;

architecture Behavioral of fifo is

    type memory_type is array (0 to ((2**ADDRESS_WIDTH)-1)) of std_logic_vector(DATA_WIDTH-1 downto 0);
    
    signal memory               : memory_type;    
    signal readptr, writeptr    : std_logic_vector(ADDRESS_WIDTH-1 downto 0);
    signal full0                : std_logic;
    signal empty0               : std_logic;
    
    signal a0, aInc, aSum, aSub : STD_LOGIC_VECTOR(1311 downto 0);


    component Adder1312 is
    Port ( 
        a       : IN STD_LOGIC_VECTOR (1311 downto 0);
        b       : IN STD_LOGIC_VECTOR (1311 downto 0);
        res     : OUT STD_LOGIC_VECTOR (1311 downto 0);
        clk     : IN STD_LOGIC;
        rst     : IN STD_LOGIC;
        
        start   : IN STD_LOGIC
    );
    end component;
    
    component Subtractor1312 is
    Port ( 
        a       : IN STD_LOGIC_VECTOR (1311 downto 0);
        b       : IN STD_LOGIC_VECTOR (1311 downto 0);
        res     : OUT STD_LOGIC_VECTOR (1311 downto 0);
        clk     : IN STD_LOGIC;
        rst     : IN STD_LOGIC;
        
        start   : IN STD_LOGIC
    );
    end component;

    component ClockPrescaler is
        port(
            clock         : in STD_LOGIC; -- 50 Mhz
            scaledClock   : out STD_LOGIC
        );
    end component;
    
    signal scaledClk: std_logic;

    begin
        full <= full0;
        empty <= empty0; 
        
        fifo0: process(clk,reset)
        begin
            if reset = '1' then            
                readptr     <= (others => '0');
                writeptr    <= (others => '0');
                empty0      <='1';
                full0       <='0';
               
            elsif rising_edge(clk) then 
            
                    if (writeptr + '1' = readptr) then 
                        full0   <=  '1';
                    else
                        full0   <=  '0';
                    end if ;
                    
                    if (readptr = writeptr) then 
                        empty0  <=  '1';

                    else
                        empty0 <= '0';

                    end if ; 
                                   
                    if (enw = '1') then 
                        memory (conv_integer(writeptr) + 1) <= datain ;
                        writeptr <= writeptr + '1' ;
                    end if ; 
                            
                    if (enr = '1') then 
--                        dataout <= (others => '0');
--                        if (readptr < "0101001") then
--                            dataout <= aSum(conv_integer(readptr)*32 + 31 downto conv_integer(readptr)*32);
--                        elsif(readptr < "1010010") then
--                            dataout <= aSub((conv_integer(readptr)-41)*32 + 31 downto (conv_integer(readptr)-41)*32);
--                        else 
--                            dataout <= memory(conv_integer(readptr));
--                        end if; 
                        if (readptr = "0000000") then
                            dataout <= "10101010101010101010101010101010";
                        elsif (readptr < "0101010") then
                            dataout <= aSum((conv_integer(readptr)-1)*32 + 31 downto (conv_integer(readptr)-1)*32);
                        elsif (readptr < "1010011") then
                            dataout <= aSub((conv_integer(readptr)-42)*32 + 31 downto (conv_integer(readptr)-42)*32);
                        else 
                            dataout <= memory(conv_integer(readptr)-1);
                        end if;
                        readptr <= readptr + '1' ;
                    end if ; 
                end if;
            
        end process;
        
        a0 <=   memory(41)
                & memory(40) & memory(39) & memory(38) & memory(37) & memory(36) & memory(35) & memory(34) & memory(33) & memory(32) & memory(31)
                & memory(30) & memory(29) & memory(28) & memory(27) & memory(26) & memory(25) & memory(24) & memory(23) & memory(22) & memory(21)
                & memory(20) & memory(19) & memory(18) & memory(17) & memory(16) & memory(15) & memory(14) & memory(13) & memory(12) & memory(11) 
                & memory(10) & memory(9) & memory(8) & memory(7) & memory(6) & memory(5) & memory(4) & memory(3) & memory(2) & memory(1);
        aInc <= memory(82) & memory(81)
                & memory(80) & memory(79) & memory(78) & memory(77) & memory(76) & memory(75) & memory(74) & memory(73) & memory(72) & memory(71) 
                & memory(70) & memory(69) & memory(68) & memory(67) & memory(66) & memory(65) & memory(64) & memory(63) & memory(62) & memory(61)
                & memory(60) & memory(59) & memory(58) & memory(57) & memory(56) & memory(55) & memory(54) & memory(53) & memory(52) & memory(51) 
                & memory(50) & memory(49) & memory(48) & memory(47) & memory(46) & memory(45) & memory(44) & memory(43) & memory(42);
        
        
        adder : Adder1312
        port map ( 
            a       => a0,
            b       => aInc,
            res     => aSum,
            clk     => scaledClk,
            rst     => reset,
            
            start   => startMagic
        );
        
        myMagicClock : ClockPrescaler
        port map (
            clock         => clk,
            scaledClock   => scaledClk
        );
        
        subtractor : Subtractor1312
        port map ( 
            a       => a0,
            b       => aInc,
            res     => aSub,
            clk     => scaledClk,
            rst     => reset,
            
            start   => startMagic
        );
        
end Behavioral;