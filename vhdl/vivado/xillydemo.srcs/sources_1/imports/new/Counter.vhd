----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12.04.2015 22:23:50
-- Design Name: 
-- Module Name: Counter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Counter is
    generic(MEMORY_WORDS_WIDTH: INTEGER := 4);
    port(
        clk         : IN STD_LOGIC;
        rst         : IN STD_LOGIC;
        mode        : IN STD_LOGIC_VECTOR(1 downto 0);
        dataBus     : IN STD_LOGIC_VECTOR(MEMORY_WORDS_WIDTH-1 downto 0);
        output      : OUT STD_LOGIC_VECTOR(MEMORY_WORDS_WIDTH-1 downto 0)
    );
end Counter;

architecture Behavioral of Counter is

    constant RESET      : STD_LOGIC_VECTOR(1 downto 0) := "00";
    constant LOAD       : STD_LOGIC_VECTOR(1 downto 0) := "01";
    constant COUNT_UP   : STD_LOGIC_VECTOR(1 downto 0) := "10";
    constant HOLD       : STD_LOGIC_VECTOR(1 downto 0) := "11";

signal cntReg, cntNext : unsigned(MEMORY_WORDS_WIDTH-1 downto 0);

begin

    process (clk, rst)
    begin
        if (rst = '1') then
            cntReg <= (others => '0');
        elsif (rising_edge(clk)) then
            cntReg <= cntNext;
        end if;
    end process;
    
    process(cntReg, mode, dataBus)
    begin
        case mode is
            when RESET =>
                cntNext <= (others => '0');
            when LOAD =>
                if (unsigned(dataBus) > 2**MEMORY_WORDS_WIDTH - 1) then
                    cntNext <= unsigned(dataBus) - 2**MEMORY_WORDS_WIDTH;
            else
                cntNext <= unsigned(dataBus);
            end if;
            when COUNT_UP =>
                if (cntReg = 2**MEMORY_WORDS_WIDTH - 1) then
                    cntNext <= (others => '0');
            else
                cntNext <= cntReg + 1;
                end if;
            when HOLD =>
                cntNext <= cntReg;
            when others =>
                cntNext <= cntReg;
        end case;
    end process;
    
    output <= STD_LOGIC_VECTOR(cntReg);

end Behavioral;
