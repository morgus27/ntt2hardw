----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/01/2016 03:22:04 PM
-- Design Name: 
-- Module Name: MyMagicFSM - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MyMagicFSM is
    generic (
        ADDRESS_WIDTH : integer:=8;---8 bit
        DATA_WIDTH : integer:=32 ---32 bit
    );
    port ( 
        clk : in std_logic;
        rst : in std_logic;
        enR : in std_logic; --enable read,should be '0' when not in use.
        enW : in std_logic; --enable write,should be '0' when not in use.
        dataOut : out std_logic_vector(DATA_WIDTH-1 downto 0); --output data
        dataIn : in std_logic_vector (DATA_WIDTH-1 downto 0); --input data
        empty : out std_logic; --set as '1' when the queue is empty
        full : out std_logic
    );
end MyMagicFSM;


architecture Behavioral of MyMagicFSM is

     type MAGIC_STATE is (IDLE, DATA_IN, DATA_OUT, WAIT_STATE);
     
     signal stateNext, stateReg                  : MAGIC_STATE;
     signal enableWrite, enableRead              : STD_LOGIC;
     signal startMagic                           : STD_LOGIC;
     signal resetSig                             : STD_LOGIC;
     
     signal magicMode                               : STD_LOGIC_VECTOR(1 downto 0);
     signal magicCounter                            : STD_LOGIC_VECTOR(6 downto 0);
     signal dataMode                                : STD_LOGIC_VECTOR(1 downto 0);
     signal dataCounter                             : STD_LOGIC_VECTOR(6 downto 0);
     
     component fifo is
         generic (
             ADDRESS_WIDTH : integer:=8;---8 bit
             DATA_WIDTH : integer:=32 ---32 bit
         ); 
         port ( 
             clk        : IN STD_LOGIC;
             reset      : IN STD_LOGIC;
             enR        : IN STD_LOGIC; 
             enW        : IN STD_LOGIC; 
             dataOut    : OUT STD_LOGIC_VECTOR(DATA_WIDTH-1 downto 0);
             dataIn     : IN STD_LOGIC_VECTOR(DATA_WIDTH-1 downto 0);
             empty      : OUT STD_LOGIC;
             full       : OUT STD_LOGIC;
             startMagic : IN STD_LOGIC
         );
     end component;
     
     component Counter is
         generic(MEMORY_WORDS_WIDTH: INTEGER := 3);
         port(
             clk         : IN STD_LOGIC;
             rst         : IN STD_LOGIC;
             mode        : IN STD_LOGIC_VECTOR(1 downto 0);
             dataBus     : IN STD_LOGIC_VECTOR(MEMORY_WORDS_WIDTH-1 downto 0);
             output      : OUT STD_LOGIC_VECTOR(MEMORY_WORDS_WIDTH-1 downto 0)
         );
     end component;
         
begin
    
    process(clk, rst)
    begin
        if (rst = '1') then
            stateReg <= IDLE;
        elsif rising_edge(clk) then
            stateReg <= stateNext;    
        end if;
    end process;
    
    process(enR, enW, magicCounter, dataCounter) 
    begin
        case stateReg is           
            when IDLE =>
                if (enW = '1') then
                    enableWrite <= '1';
                    enableRead  <= '0';
                    empty <= '1';
                    full <= '0';
                    startMagic <= '0';
                    magicMode <= "00";
                    dataMode <= "10";
                    resetSig <= '0';
                    stateNext   <= DATA_IN;
                else
                    enableWrite <= '0';
                    enableRead  <= '0';
                    empty <= '1';
                    full <= '0';
                    startMagic <= '0';
                    magicMode <= "00";
                    dataMode <= "00";
                    resetSig <= '1';
                    stateNext   <= IDLE;                     
                end if;                    
            when DATA_IN =>     
                if (dataCounter = "1010010") then
                    enableWrite <= '0';
                    enableRead  <= '0';
                    empty <= '1';
                    full <= '1';
                    startMagic <= '1';
                    magicMode <= "10";
                    dataMode <= "00";
                    resetSig <= '0';
                    stateNext   <= WAIT_STATE; 
                else 
                    if (enW = '1') then
                        enableWrite <= '1';
                        enableRead  <= '0';
                        empty <= '1';
                        full <= '0';
                        startMagic <= '0';
                        magicMode <= "00";
                        dataMode <= "10";
                        resetSig <= '0';
                        stateNext   <= DATA_IN;
                        
                    else
                        enableWrite <= '0';
                        enableRead  <= '0';
                        empty <= '1';
                        full <= '0';
                        startMagic <= '0';
                        magicMode <= "00";
                        dataMode <= "11";
                        resetSig <= '0';
                        stateNext   <= DATA_IN;       
                    end if;    
                end if;                  
            when WAIT_STATE =>             
                if (magicCounter = "0110000") then
                    enableWrite <= '0';         
                    full <= '1';
                    empty <= '0';
                    startMagic <= '1';
                    magicMode <= "11";
                    resetSig <= '0';
                    if (enR='1') then
                        stateNext   <= DATA_OUT;
                        enableRead  <= '1';
                        dataMode <= "10";                      
                    else
                        stateNext   <= WAIT_STATE;
                        enableRead  <= '0';
                        dataMode <= "00";
                    end if;
                else
                    enableWrite <= '0';
                    enableRead  <= '0';
                    empty <= '1';
                    full <= '1';
                    startMagic <= '1';                    
                    magicMode <= "10";
                    dataMode <= "00";
                    resetSig <= '0';
                    stateNext   <= WAIT_STATE; 
                end if;      
            when DATA_OUT =>
                if (dataCounter = "1010110") then --1010010
                    enableWrite <= '0';
                    enableRead  <= '0';
                    empty <= '1';
                    full <= '0';
                    startMagic <= '0';
                    magicMode <= "00";
                    dataMode <= "00";
                    resetSig <= '1';
                    stateNext   <= IDLE; 
                else
                    if (enR='1') then
                        enableWrite <= '0';
                        enableRead  <= '1';
                        empty <= '0';
                        full <= '1';
                        startMagic <= '1';
                        magicMode <= "00";
                        dataMode <= "10";
                        resetSig <= '0';
                        stateNext   <= DATA_OUT; 
                    else
                        enableWrite <= '0';
                        enableRead  <= '0';
                        empty <= '0';
                        full <= '1';
                        startMagic <= '1';
                        magicMode <= "00";
                        dataMode <= "11";
                        resetSig <= '0';
                        stateNext   <= DATA_OUT; 
                    end if;
                end if;
        end case;
    end process;
    
    internalFifo : fifo
    generic map (
        ADDRESS_WIDTH => 7,
        DATA_WIDTH => 32
    )
    port map ( 
        clk         => clk,
        reset       => resetSig,
        enr         => enableRead,
        enw         => enableWrite,
        dataout     => dataOut,
        datain      => dataIn,
        empty       => open,
        full        => open,
        startMagic  => startMagic
    );


    magicCounterInst: Counter
    generic map(MEMORY_WORDS_WIDTH => 7)
    port map(
        clk        => clk,
        rst        => resetSig,
        mode       => magicMode,
        dataBus    => "0000000",
        output     => magicCounter
    );
    
    dataCounterInst: Counter
    generic map(MEMORY_WORDS_WIDTH => 7)
    port map(
        clk        => clk,
        rst        => resetSig,
        mode       => dataMode,
        dataBus    => "0000000",
        output     => dataCounter
    );
     
end Behavioral;
