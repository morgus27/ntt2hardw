----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/02/2016 10:35:39 AM
-- Design Name: 
-- Module Name: Subtractor1312 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Subtractor1312 is
    Port ( 
        a       : IN STD_LOGIC_VECTOR (1311 downto 0);
        b       : IN STD_LOGIC_VECTOR (1311 downto 0);
        res     : OUT STD_LOGIC_VECTOR (1311 downto 0);
        clk     : IN STD_LOGIC;
        rst     : IN STD_LOGIC;
        
        start   : IN STD_LOGIC
    );
end Subtractor1312;

architecture Behavioral of Subtractor1312 is

    signal aReg, bReg        : STD_LOGIC_VECTOR(1311 downto 0);
    signal resReg, resSig    : STD_LOGIC_VECTOR(1311 downto 0);
    

begin

    process (clk, rst)
    begin
        if (rst = '1') then
            resReg <= (others => '0');
            aReg <= (others => '0');
            bReg <= (others => '0');
            
        elsif (rising_edge(clk)) then
            resReg <= resSig;
            aReg <= a;
            bReg <= b;
                                        
        end if;
    end process;
    
    process(start)
    begin
        resSig <= aReg - bReg;
    end process;

    res <= resReg;
        
end Behavioral;
